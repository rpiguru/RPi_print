## Register printer to CUPS service

1. Install dependencies

- Expand file system with `sudo raspi-config` command and update packages:
        
        sudo apt-get update
        
- Install dependencies

        sudo apt-get install cups build-essential libcups2-dev libcupsimage2-dev automake

2. Capable printers    
    
    - HP LaserJet 1018
    
        http://andrum99.blogspot.nl/2013/06/getting-hp-laserjet-1018-printer.html
    
    - HP P1005
    
        https://mark.zealey.org/2015/06/29/getting-hp-p1005-and-associated-laserjet-printers-working-with-raspberry-pi
    
    - Brother DCP-7030, Brother DCP-7055, Brother DCP-7055W, Brother DCP-7065DN
     
        https://www.raspberrypi.org/forums/viewtopic.php?f=63&t=116509
    
    - Brother HL-L2360DN
        
        https://www.raspberrypi.org/forums/viewtopic.php?f=91&t=109609&p=761801
    
    
3. Working with **Brother DCP-7030**
    
        git clone https://github.com/pdewacht/brlaser.git
        cd brlaser
        ./autogen.sh
        ./configure
        make
        sudo make install
    
    Now register printer by following:
    
    http://www.howtogeek.com/169679/how-to-add-a-printer-to-your-raspberry-pi-or-other-linux-computer/
    
4. Working with **HP LaserJet P1006**
    
    Before starting, please check **usblp** with this:
     
        sudo modprobe usblp
    
    If fails, we need to recompile usb kernel of RPi following bellow.
        
    https://www.raspberrypi.org/forums/viewtopic.php?f=66&t=15313
    
    And then let us start.
    
        $ sudo apt-get install cups
        $ sudo usermod -a -G lpadmin pi
        
        $ wget -O foo2zjs.tar.gz http://foo2zjs.rkkda.com/foo2zjs.tar.gz
        $ tar zxf foo2zjs.tar.gz
        $ cd foo2zjs
        
        $ make
        
        $ ./getweb P1006
        
        $ sudo make install
        $ sudo make install-hotplug
        $ sudo make cups
    
5. Working with Zebra Label Printer(Not tested.)
    
        sudo apt-get install printer-driver-gutenprint
        
6. Working with Brother HL-5450DNT
    
    I have installed driver of `Brother HL-5170DN` and it worked.
    
7. Install **Imagemagick** to combine 2 pdf files.
    
        sudo apt-get install imagemagick
    
     
## Working with python

1. Install dependencies

        sudo apt-get install python-dev
        sudo pip install pycups
       
2. Print a file with CUPS driver
    
        import cups
        conn = cups.Connection()
        printers = conn.getPrinters()
        
        # Display discovered printers.
        for printer in printers:
            print printer, printers[printer]["device-uri"]
        
        file = 'test.pdf'
        
        printer_name = printers.keys()[0]
         
        print "Printing, return value: ", conn.printFile(printer_name, file, "Test Print", {})

3. Install dependencies for pdf merge.

        sudo apt-get install imagemagick
        sudo pip install pypdf2
        
4. Install dependencies for Kivy APP.

- Install driver for WaveShare 3.5" touch screen
    
        cd ~
        wget http://www.waveshare.com/w/upload/3/3d/LCD-show-160811.tar.gz
        tar xvf LCD-show-160811.tar.gz
        cd LCD-show/
        ./LCD35-show
        
    After rebooting, RPi LCD is ready to use.
    
- Install kivy
    
        sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core \
            gstreamer1.0-plugins-{bad,base,good,ugly} \
            gstreamer1.0-{omx,alsa} python-dev
        
        sudo pip install Cython==0.20
        
        sudo apt-get install python-kivy
    
    Open the kivy configuration file with `nano .kivy/config.ini` and add line under the `[input]` line:
        
        [input]
        %(name)s = probesysfs,provider=hidinput
    
    And change some values:
        
        width = 480
        height = 320
        
        keyboard_mode = dock
        
- Install dependencies for Kivy APP
    
        sudo pip install wifi
        
- Give full permission the the wifi setting file

        sudo chmod 777 /etc/network/interfaces

- Copy config file to root.

        sudo cp /home/pi/.kivy/config.ini /root/.kivy/config.ini

- Run APP.
    
        DISPLAY=:0.0 python main.py
 
- Prevent Screen Saver.
    
        sudo apt-get install x11-xserver-utils
        sudo nano /etc/X11/xinit/xinitrc
    
    Add following at the end of the file.

        xset s off         # don't activate screensaver
        xset -dpms         # disable DPMS (Energy Star) features.
        xset s noblank     # don't blank the video device
        
    Change another file.

        sudo nano /etc/lightdm/lightdm.conf

    In the *SeatDefaults* section it gives the command for starting the X server which 
    
    I modified to get it to turn off the screen saver as well as dpms.
    
        [SeatDefaults]
        xserver-command=X -s 0 -dpms
    
- Create desktop shortcut. 
    
        nano ~/Desktop/Printer.desktop
    
    And add this:
    
        [Desktop Entry]
        Name=RPi Printer
        Comment=Invoice Printer
        Exec=/usr/bin/python /home/pi/rpi_printer/main.py
        Type=Application
        Encoding=UTF-8
        Terminal=true
        Categories=None;

- Make desktop icon larger.
    
        nano ~/.config/libfm/libfm.conf

    If there is no such file, open another file.
    
        sudo nano /etc/xdg/libfm/libfm.conf

    In the `[ui]` section, change the `pane_icon_size` to 48.

- Enable single click.

        sudo nano .config/libfm/libfm.conf

    Change 
    
        single_click=0

    to
    
        single_click=1

    
- Enable auto-start app
    
        sudo nano /etc/profile
    
    And add this at the end of file
        
        /usr/bin/sudo /usr/bin/python /home/pi/rpi_printer/main.py
    

         