# -*- coding: utf-8 -*-
"""
Date : 13/08/2016

"""
import glob
import os
import threading
import subprocess
import Queue
import xml.etree.ElementTree
import time
import wifi
import socket
import struct

from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, ListProperty, ObjectProperty
from kivy.uix.screenmanager import Screen, NoTransition
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition
from kivy.clock import Clock
import pdf_utils
from base import Base
from printer import RPiPrinter

try:
    import cups
    import fcntl
except ImportError:
    print 'Failed to import cups package.'


class WiFiCtrl:

    def __init__(self):
        pass

    def get_ap_list(self):
        try:
            l_cell = wifi.Cell.all('wlan0')
        except wifi.exceptions.InterfaceError:
            # When wlan0 is down, turn it on and scan again.
            subprocess.call(['/sbin/ifup wlan0'], shell=True)
            l_cell = wifi.Cell.all('wlan0')

        ap_list = []
        for cell in l_cell:
            ap_list.append(cell.ssid)
        return ap_list


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class MainScreen(Screen):
    fullscreen = BooleanProperty(False)

    def add_widget(self, *args):
        if 'content' in self.ids:
            return self.ids.content.add_widget(*args)
        return super(MainScreen, self).add_widget(*args)


class MainApp(App, Base):
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty([])
    screens = {}                            # Dict of all screens
    hierarchy = ListProperty([])

    # Popup instances
    caution_popup = ObjectProperty(None)
    confirm_popup = ObjectProperty(None)

    confirm_param = ListProperty([])        # Parameter used in 'confirm popup'

    inst_wifi = ObjectProperty(None)                        # wifi instance
    dropdown = DropDown()                   # DropDown instance in 'WiFi Setting' screen

    inst_printer = ObjectProperty(None)
    pending_queue = ObjectProperty(None)    # Queue contains pending files... (file_name, print_type)

    pdf_data = ObjectProperty(None)
    b_lock = BooleanProperty(False)

    b_stop = BooleanProperty(False)

    pdf_downloader = ObjectProperty(None)

    delivered_list = ListProperty([])

    def build(self):
        """
        base function of kivy app
        :return:
        """

        Base.__init__(self)

        self.load_screen()

        self.caution_popup = CautionPopup()
        self.confirm_popup = ConfirmPopup()

        self.confirm_param = [None for i in range(5)]

        self.inst_wifi = WiFiCtrl()

        self.inst_printer = RPiPrinter()

        self.pending_queue = Queue.Queue()

        self.pdf_downloader = pdf_utils.PDFDownloader()

        self.go_screen('entry', 'right')

        # self.go_screen('menu', 'right')
        # threading.Thread(target=self.receive_pdf).start()

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

        if self.confirm_param:
            if self.confirm_param[0] == 'connect_to_ap':
                self.connect_to_ap(self.confirm_param[1], self.confirm_param[2])
            elif self.confirm_param[0] == 'update_token':
                new_token = self.screens['settings'].ids['txt_token'].text
                self.set_param_to_xml('TOKEN', new_token)

    def confirm_no(self):
        """
        callback function of "No" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()
        if self.confirm_param[0] == 'set_relay_from_maintenance':
            self.confirm_param[3].state = 'normal'
            self.confirm_param[4].state = 'down'
        elif self.confirm_param[0] == 'update_token':
            self.screens['settings'].ids['txt_token'].text = ''

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        sm = self.root.ids.sm

        if 'settings' in dest_screen and 'settings' in self.current_title:
            sm.transition = NoTransition()
        else:
            sm.transition = SlideTransition()

        if dest_screen == 'settings_wifi':
            self.update_ap_list()
        elif dest_screen == 'menu':
            Clock.schedule_interval(self.pdf_watchdog, 1)
            self.caution_popup.dismiss()
        elif dest_screen == 'settings':
            self.screens['settings'].ids['txt_token'].text = self.get_param_from_xml('TOKEN')

        screen = self.screens[dest_screen]
        sm.switch_to(screen, direction=direction)
        self.current_title = screen.name

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob(self.cur_dir + "screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        return True

    def update_ap_list(self):
        """
        Get surround WiFi AP list and update dropdown widget.
        :return:
        """
        ap_list = self.inst_wifi.get_ap_list()
        # ap_list = ['AP 1', 'AP 2', 'AP 3', 'AP 4']
        self.dropdown.clear_widgets()

        for ap_name in ap_list:
            btn = Button(text=ap_name, size_hint_y=None, height=30)
            btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))
            self.dropdown.add_widget(btn)

        main_btn = self.screens['settings_wifi'].ids['btn_dropdown']
        main_btn.bind(on_release=self.dropdown.open)

        self.dropdown.bind(on_select=lambda instance, x: setattr(main_btn, 'text', x))
        if len(ap_list) > 0:
            cur_ap = self.get_current_ap()
            if cur_ap != '' and cur_ap in ap_list:
                main_btn.text = cur_ap
            else:
                main_btn.text = ''

    def btn_connect_to_ap(self):
        """
        This function is called when user presses 'Connect' button.
        :return:
        """
        pwd = self.screens['settings_wifi'].ids['txt_pwd'].text
        ssid = self.screens['settings_wifi'].ids['btn_dropdown'].text
        self.confirm_popup.ids['lb_content'].text = 'Connect to ' + ssid + "?\nIt will take a while"
        self.confirm_param[0] = 'connect_to_ap'
        self.confirm_param[1] = ssid
        self.confirm_param[2] = pwd
        self.confirm_popup.open()

    def connect_to_ap(self, ssid, pwd):
        """
        Modify /etc/network/interface file and turn off/on wlan0 interface with ifdown/ifup shell command
        :param ssid: New SSID of ap
        :param pwd: password
        :return:
        """
        # ---- modify /etc/network/interface file ----
        f = open('/etc/network/interfaces', 'r+b')
        f_content = f.readlines()
        line_num = 0
        for i in range(len(f_content)):
            if 'iface wlan0 inet' in f_content[i]:
                line_num = i
                break
        f_content[line_num] = 'iface wlan0 inet dhcp\n'
        try:
            f_content[line_num + 1] = '    wpa-ssid ' + ssid
        except IndexError:
            f_content.append('    wpa-ssid ' + ssid)

        try:
            f_content[line_num + 2] = '    wpa-psk ' + pwd
        except IndexError:
            f_content.append('    wpa-psk ' + pwd)

        for i in range(len(f_content)):
            if 'wpa-conf /etc/wpa' in f_content[i] and '#' not in f_content[i]:
                f_content[i] = "#  " + f_content[i]

        f.seek(0)
        f.truncate()
        f.write(''.join(f_content))
        f.close()
        os.system('/usr/bin/sudo /sbin/ifdown wlan0')
        os.system('/usr/bin/sudo /sbin/ifup wlan0')

        ip = self.get_ip_address('wlan0')
        if ip is not None:
            self.caution_popup.ids['lb_content'].text = "New IP: " + ip
            self.caution_popup.open()
            return True
        else:
            self.caution_popup.ids['lb_content'].text = "Failed to connect to AP\nPlease check SSID & password"
            self.caution_popup.open()
            return False

    @staticmethod
    def get_current_ap():
        """
        Get currently connected AP's SSID
        :return:
        """
        pipe = os.popen('iwgetid -r')
        data = pipe.read().strip()
        pipe.close()
        return data

    @staticmethod
    def get_ip_address(ifname):
        """
        Get assigned IP address of given interface
        :param ifname: interface name such as wlan0, eth0, etc
        :return:
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
            return ip
        except IOError:
            return None

    def update_settings(self):
        """
        Update settings to the config file...
        :return:
        """
        new_token = self.screens['settings'].ids['txt_token'].text
        if new_token.strip() == '':
            self.caution_popup.ids['lb_content'].text = 'Please input new token'
            self.caution_popup.open()
        else:
            self.confirm_popup.ids['lb_content'].text = 'Would you like to update settings?'
            self.confirm_param = ['update_token', ]
            self.confirm_popup.open()

    def pdf_watchdog(self, *args):
        """
        Monitor the Queue, it anything is received, print it!
        NOTE: This function is called every second
        :return:
        """
        status_widget = self.screens['menu'].ids['lb_status']

        if not self.pending_queue.empty() and not self.b_lock:
            self.b_lock = True
            self.pdf_data = self.pending_queue.get()
            print "Received PDF file, metadata: ", self.pdf_data.items()
            a = pdf_utils.PDFMerge(pdf_file=self.pdf_data['pdf_file'])
            if self.pdf_data['mode'] != 'regular':
                a.merge_pdf()
                self.pdf_data['pdf_file'] = a.merged_file

            # Pop from the delivered list when fails to print file.
            if not self.print_doc(self.pdf_data['pdf_file']):
                self.delivered_list.remove(self.pdf_data['created_at'])

        else:
            if status_widget.text.count('.') < 4:
                status_widget.text += "."
            else:
                status_widget.text = 'Waiting for pdf file'

        if self.b_lock:
            status_widget.text = 'Printing...'
        else:
            status_widget.text = 'Waiting for new invoice...'

    def start_printer(self, wid):
        if wid.text == 'Start':
            wid.text = 'Stop'
            self.b_stop = False
            print "Started to receive PDF file...."
            threading.Thread(target=self.receive_pdf).start()
        else:
            wid.text = 'Start'
            self.b_stop = True

    def print_doc(self, pdf_file):
        """
        Print document
        :return:
        """
        print "Printing pdf file..."
        p_list = self.inst_printer.get_printer_list()

        if len(p_list) == 0:
            self.caution_popup.ids['lb_content'].text = 'Error\nNo printer found.'
            self.caution_popup.open()
            print "No printer found..."
            return False

        self.inst_printer.print_file(p_list[0], pdf_file)

        self.b_lock = False

        return True

    def receive_pdf(self, *args):
        """
        Receive pdf file from the web server via REST API and push its metadata to the Queue
        :return:
        """
        while True:
            if self.b_stop:
                break

            s_time = time.time()
            data = self.pdf_downloader.download_to_file()

            if data is not None:
                if len(self.delivered_list) == 0:
                    for received_data in data['data']:
                        self.pending_queue.put(received_data)
                        self.delivered_list.append(received_data['created_at'])

                else:       # Check whether received pdf file was already delivered before.
                    for received_data in data['data']:
                        if received_data['created_at'] not in self.delivered_list:
                            self.pending_queue.put(received_data)
                            self.delivered_list.append(received_data['created_at'])

            elapsed = time.time() - s_time
            print "Elapsed: ", elapsed


if __name__ == '__main__':

    app = MainApp()
    app.run()
